<?php

define(REFERER_THEME,           'referer_theme');
define(REFERER_THEME_TIMESTAMP, 'referer_theme_timestamp');
define(REFERER_THEME_THEME,     'referer_theme_theme');
define(REFERER_THEME_INTERVAL,  'referer_theme_interval');
define(REFERER_THEME_MAX_SITES, 3);
// Number of seconds to keep the site in the assigned theme
// for the current visitor. Default is 3 hours
define(REFERER_THEME_PERIOD,    60*60*3);

function referer_theme_help($section) {
  switch ($section) {
    case 'admin/modules#description':
    case 'admin/settings/referer_theme':
    case 'admin/help#referer_theme':
      return t('This module changes the site to a different theme for each visitor based on referer.');
  }
}

function referer_theme_admin_settings() {
  $theme_list = array();
  foreach(list_themes() as $key => $theme) {
    if ($theme->status == 1) {
      $theme_list[$key] = $theme->name;
    }
  }

  for($i=1; $i<REFERER_THEME_MAX_SITES+1; $i++) {
    $set = 'host' . $i;
    $form[$set] = array(
      '#type' => 'fieldset',
      '#title' => t('Host ' . $i),
      '#description' => t(''),
    );
    
    $param = REFERER_THEME . '_site_' . $i;
    $form[$set][$param] = array(
      '#type' => 'textfield',
      '#title' => t('Site host name'),
      '#default_value' => variable_get($param, ''),
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => t('The host name of the site that is allowed to referer_theme. This is the part after http:// and before the first /.'),
    );

    $param = REFERER_THEME . '_theme_' . $i;
    $form[$set][$param] = array(
      '#type' => 'select',
      '#title' => t('Site theme'),
      '#default_value' => variable_get($param, ''),
      '#options' => $theme_list,
    );
  }

  return system_settings_form($form);
}


function referer_theme_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path'               => 'admin/settings/referer_theme',
      'title'              => t('Referer theme'),
      'description'        => t('Referer theme settings'),
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('referer_theme_admin_settings'),
      'access'             => user_access('administer site configuration'),
      'type'               => MENU_NORMAL_ITEM, // optional
    );


    // Check if we should force a reset
    if ($_GET[REFERER_THEME] == 'false') {
      _referer_theme_session_reset();
      return $items;
    }

    // Check if the time interval expired
    _referer_theme_session_check_reset();

    // Check if we already have an referer_theme theme
    if (_referer_theme_session_get()) {
      return $items;
    }

    // Check the referer and set the referer_theme if applicable
    _referer_theme_site_match();
    return $items;
  }

  return $items;
}

function _referer_theme_site_match() {
  $page_match = false;

  for($i=1; $i<REFERER_THEME_MAX_SITES+1; $i++) {
    $site = variable_get(REFERER_THEME . '_site_' . $i, '');
    if ($site) {
      if (_referer_theme_check_referer($site, $i)) {
        $page_match = true;
        break;
      }
    }
  }

  return $page_match;
}

function _referer_theme_check_referer($site, $index) {
  $rc = false;
  $protocol = ($_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
  $pattern = preg_quote("$protocol://$site", '/');
  $pattern = '/^' . $pattern . '/';
  $referer = $_SERVER['HTTP_REFERER'];
  if (preg_match($pattern, $referer)) {
    _referer_theme_session_set($index);
    $rc = true;
  }
  return $rc;
}

function _referer_theme_session_get() {
  global $custom_theme;

  if ($_SESSION[REFERER_THEME_TIMESTAMP]) {
    $custom_theme = $_SESSION[REFERER_THEME_THEME];
    return true;
  }

  return false;
}

function _referer_theme_session_set($site_index) {
  global $custom_theme;

  $theme = variable_get(REFERER_THEME . '_theme_' . $site_index, '');
  $_SESSION[REFERER_THEME_TIMESTAMP] = time();
  $_SESSION[REFERER_THEME_THEME] = $theme;

  $custom_theme = $theme;
}

function _referer_theme_session_reset() {
  unset($_SESSION[REFERER_THEME_TIMESTAMP]);
  unset($_SESSION[REFERER_THEME_THEME]);
}

function _referer_theme_session_check_reset() {
  $timestamp = $_SESSION[REFERER_THEME_TIMESTAMP] + variable_get(REFERER_THEME_INTERVAL, REFERER_THEME_PERIOD);
  $now = time();
  if ($timestamp > 0 && $timestamp <= $now) {
    _referer_theme_session_reset();
    return true;
  }

  return false;
}

