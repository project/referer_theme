
Copyright 2006 http://2bits.com

Description
-----------
This module provides web sites that offer a service the ability to have
different themes depending on the referer the user is coming from.

For example, if a site offers outsourced ecommerce, or event registration,
they can have a branded theme for their clients with each client's logo.
If a visitor clicks on a link from the client site, they will be assigned
a theme for a certain time.

Other visitors coming from different referers will have different themes
and are unaffected.

Prerequisites
-------------
You must develop a theme for every client you have and install them normally
as you would with any other theme, and enable them as well.

The themes can be made to fit the original (refering) site's look and feel.
For example, have the same logo, color, fonts, ...etc. You can also suppress
one side bar or both if you so with. In short, anything that can be done
in a theme is allowed here.

Using phptemplate is recommended.

Installation
------------
This module requires no database changes.

To install, copy the referer_theme.module to your modules directory.

Configuration
-------------
To enable this module, visit Administer -> Modules, and enable referer_theme.

To configure it, go to Administer -> Settings -> referer_theme.

For each site, enter the hostname that visitor would come from. This is the
part between the "http://" and the first "/"

Assign a theme for that site.

Advanced:
---------
The default number of sites to configure is 3. If you want to increase
this, then change the value for REFERER_THEME_MAX_SITES at the top of
the module using an editor.

The default time for a theme to remain is 3 hours from the time the
user first visited. If you want to adjust that time, then adjust the
value REFERER_THEME_PERIOD at the top of the site.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
